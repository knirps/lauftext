from st3m.application import Application, ApplicationContext
from st3m.input import InputController
import st3m.run
import leds
import json

class TextConfig:
    text: str
    color: (int, int, int)
    bg_color: (int, int, int)
    speed: int
    # TODO font, font size, LED stuff, etc

    def __init__(self, text: str, color: (int, int, int), bg_color: (int, int, int) = (0, 0, 0), speed: int = -100):
        self.text = text
        self.color = color
        self.bg_color = bg_color
        self.speed = speed


def errtxt(msg: str) -> [TextConfig]:
    return [TextConfig(msg, (255, 0, 0))]


def parse_config(fp) -> [TextConfig]:
    raw = json.load(fp)
    if type(raw) is not list:
        return errtxt("top level element must be a list")
    return list(map(parse_text_config, raw))


def parse_text_config(d) -> TextConfig:
    if type(d) is not dict:
        return errtxt("config list elements must be objects")

    text = d.get("text", None)
    if type(text) is not str:
        return errtxt("text is a mandatory string")

    text_color = d.get("color", [255, 255, 255])
    if type(text_color) is not list or len(text_color) != 3:
        return errtxt("color must be a list of three integers")
    if (
        type(text_color[0]) is not int
        or type(text_color[1]) is not int
        or type(text_color[2]) is not int
    ):
        return errtxt("color values must be integers")

    bg_color = d.get("bg_color", [0, 0, 0])
    if type(bg_color) is not list or len(bg_color) != 3:
        return errtxt("color must be a list of three integers")
    if (
        type(bg_color[0]) is not int
        or type(bg_color[1]) is not int
        or type(bg_color[2]) is not int
    ):
        return errtxt("color values must be integers")

    speed = d.get("speed", -100)
    if type(speed) is not int:
        return errtxt("speed must be an integer")

    return TextConfig(text, (text_color[0], text_color[1], text_color[2]), (bg_color[0], bg_color[1], bg_color[2]), speed)

def fallback_texts() -> [TextConfig]:
    return [
        TextConfig("42", (255, 255, 255)),
        TextConfig("Das geht nicht in Saal 1", (200, 0, 0)),
        TextConfig("You are filled with determination", (255, 255, 255)),
        TextConfig("Camp is like Congress, only more in tents", (251, 72, 196)),
        TextConfig("Stay hydrated!", (0, 75, 255)),
        TextConfig(
            "Never gonna give you up, never gonna let you down, never gonna run around and desert you. Never gonna make you cry, never gonna say goodbye, never gonna tell a lie and hurt you.",
            (200, 200, 200),
        ),
        TextConfig("So long and thanks for all the fish", (255, 255, 255)),
        TextConfig("I ATE'NT DEAD", (123, 123, 0)),
    ]


def read_config(filename: str):
    try:
        with open(filename) as f:
            texts = parse_config(f)
            if len(texts) == 0:
                return fallback_texts()
            return texts
    except OSError:
        # FileNotFoundError does not seem to exist in this environment,
        # so we use OSError and just assume it is because the file is missing
        # if the user has not created a config file yet, just hand out some default texts
        return fallback_texts()
    except ValueError as e:
        return errtxt(f"baaaad json: {e}")


class Ticker(Application):
    def __init__(self, ctx: ApplicationContext) -> None:
        super().__init__(ctx)
        self.input = InputController()
        self.text_id = 0
        self.text_width = 0.0
        self.offset = 0.0
        self.texts = read_config("/sd/lauftextcfg.json")

    def draw(self, ctx: Context) -> None:
        cfg = self.texts[self.text_id]

        (r, g, b) = (255, 255, 255)
        if cfg.color != None:
            (r, g, b) = cfg.color
        (bg_r, bg_g, bg_b) = cfg.bg_color

        ctx.rgb(bg_r / 255, bg_g / 255, bg_b / 255).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(r / 255, g / 255, b / 255)
        ctx.font = "Camp Font 1"
        ctx.font_size = 48
        self.text_width = ctx.text_width(cfg.text)
        ctx.move_to(self.offset, 0)
        ctx.text(cfg.text)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        cfg = self.texts[self.text_id]

        if self.input.buttons.app.left.pressed:
            self.text_id = (self.text_id + 1) % len(self.texts)
            self.offset = 0
        elif self.input.buttons.app.right.pressed:
            self.text_id = (self.text_id - 1) % len(self.texts)
            self.offset = 0

        if cfg.speed > 0 and self.offset > 120:
            self.offset = -120 - self.text_width
        elif cfg.speed < 0 and self.offset + self.text_width < -120:
            self.offset = 120
        else:
            self.offset += cfg.speed * delta_ms / 1000


if __name__ == "__main__":
    st3m.run.run_view(Ticker(ApplicationContext()))
