Lauftext
========

Version 2

A flow3r app that displays some ticker text.


Usage
-----

Use the left button to switch between displayed texts.

Configuration File
------------------

The texts can be configured via a configuration file `lauftextcfg.json` on the SD card.

There is an example config file in this repository: `lauftextcfg.json.example`.

Only the `text` field is mandatory, all other field will fall back to some reasonable default value.

Please make sure the json does not contain line breaks, for some reason the parser cannot handle this.

```
[{"text":"The cake is a lie!","color":[255,128,0]},{"text":"Where are the Snowdens of yesteryear?", "speed":-150, "bg_color":[50, 50, 50]}]
```

Development
-----------

I plan to do some more features, such as configurable scrolling font, font size, etc. However, I have to do with Real Life™, so that may take some time.

PRs are welcome, but I may be picky.
